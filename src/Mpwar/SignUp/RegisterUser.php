<?php

namespace Mpwar\SignUp;

use Mpwar\SignUp\Domain\Email;
use Mpwar\SignUp\Domain\Password;
use Mpwar\SignUp\Domain\User;
use Mpwar\SignUp\Event\Event;

class RegisterUser
{

    const MIN_PASSWORD_LENGTH = 6;

    private $repository;
    private $queue;

    public function __construct(UserRepository $repository, EventQueue $queue) {
        $this->repository = $repository;
        $this->queue      = $queue;
    }

    public function registerNew($raw_email, $raw_password)
    {
        $user = new User(
            new Email($raw_email),
            new Password($raw_password)
        );
        $this->repository->add($user);

        $newUserEvent = new Event('NewUser');
        $this->queue->dispatch($newUserEvent);

        $autoLoginUserEvent = new Event('AutoLoginUser');
        $this->queue->dispatch($autoLoginUserEvent);
    }

}
