<?php

namespace Mpwar\SignUp;

use Mpwar\SignUp\Domain\User;

interface UserRepository
{
    public function add(User $user);
}
