<?php

namespace Mpwar\SignUp\Domain;

class User
{

    private $email;
    private $password;

    public function __construct(Email $email, Password $password)
    {
        $this->email    = $email;
        $this->password = $password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

}
