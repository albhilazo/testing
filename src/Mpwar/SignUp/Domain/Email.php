<?php

namespace Mpwar\SignUp\Domain;

class Email
{

    private $value;

    public function __construct($email)
    {
        if ($this->emailIsNotValid($email))
        {
            throw new \Mpwar\SignUp\Exception\InvalidEmailException();
        }

        $this->value = $email;
    }

    public function getValue()
    {
        return $this->value;
    }

    private function emailIsNotValid($email)
    {
        $isValid = filter_var($email, FILTER_VALIDATE_EMAIL);
        return ($isValid == false);
    }

}
