<?php

namespace Mpwar\SignUp\Domain;

class Password
{

    const MIN_PASSWORD_LENGTH = 6;

    private $value;

    public function __construct($password)
    {
        if ($this->passwordIsTooShort($password))
        {
            throw new \Mpwar\SignUp\Exception\PasswordTooShortException();
        }

        if ($this->passwordHasNoUppercase($password))
        {
            throw new \Mpwar\SignUp\Exception\PasswordMissingUppercaseException();
        }

        if ($this->passwordHasNoNumbers($password))
        {
            throw new \Mpwar\SignUp\Exception\PasswordMissingNumbersException();
        }

        //$this->value = password_hash($password, PASSWORD_BCRYPT);
        $this->value = sha1($password);
    }

    public function getValue()
    {
        return $this->value;
    }

    private function passwordIsTooShort($password)
    {
        $passwordLength = strlen($password);
        return ($passwordLength < self::MIN_PASSWORD_LENGTH);
    }

    private function passwordHasNoUppercase($password)
    {
        return (strtolower($password) == $password);
    }

    private function passwordHasNoNumbers($password)
    {
        $allCharactersAlphabetic = ctype_alpha($password);
        return ($allCharactersAlphabetic == true);
    }

}
