<?php

namespace Mpwar\SignUp\Event;

class Event
{

    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

}
