<?php

namespace Mpwar\SignUp;

use Mpwar\SignUp\Event\Event;

interface EventQueue
{
    public function dispatch(Event $event);
}
