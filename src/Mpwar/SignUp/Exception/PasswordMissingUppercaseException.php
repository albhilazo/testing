<?php

namespace Mpwar\SignUp\Exception;

final class PasswordMissingUppercaseException extends InvalidPasswordException {}
