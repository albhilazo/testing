<?php

namespace Mpwar\SignUp\Exception;

final class PasswordMissingNumbersException extends InvalidPasswordException {}
