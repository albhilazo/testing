<?php

namespace Mpwar\SignUp\Exception;

final class PasswordTooShortException extends InvalidPasswordException {}
