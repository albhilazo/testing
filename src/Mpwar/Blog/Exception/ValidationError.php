<?php

namespace Mpwar\Blog\Exception;

// Do not modify this file at all

use RuntimeException;

final class ValidationError extends RuntimeException {}
