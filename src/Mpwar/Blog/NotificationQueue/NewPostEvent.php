<?php

namespace Mpwar\Blog\NotificationQueue;

// Do not modify this file at all

interface NewPostEvent
{
    public function publishLastInsertedPost();
}
