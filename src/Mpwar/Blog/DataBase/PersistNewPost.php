<?php

// Do not modify this file at all

namespace Mpwar\Blog\DataBase;

interface PersistNewPost
{
    public function persist($title, $body);
}
