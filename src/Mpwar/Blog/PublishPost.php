<?php

namespace Mpwar\Blog;

use Mpwar\Blog\DataBase\PersistNewPost;
use Mpwar\Blog\NotificationQueue\NewPostEvent;
use Mpwar\Blog\Exception\ValidationError;

class PublishPost
{

    private $persist_service;
    private $event_service;




    public function __construct(/*PersistNewPost $persist_service, NewPostEvent $event_service*/)
    {
        /*$this->persist_service = $persist_service;
        $this->event_service   = $event_service;*/
    }

    public function __invoke($title, $body, $publishNow)
    {
        if (strlen($title) > 50)
        {
            throw new ValidationError();
        }

        if (strlen($body) > 1000)
        {
            throw new ValidationError();
        }
    }
}
