<?php

namespace Mpwar\Money;

class Money
{

    private $available_currency;




    public function __construct($available_currency)
    {
        $this->available_currency = $available_currency;
    }

    public function getBreakdown($number)
    {
        $available_currency_size = count($this->available_currency);
        $remaining_amount = $number;
        $breakdown = array();
        while ($remaining_amount > $this->available_currency[$available_currency_size-1])
        {
            for ($currency_slot=0; $currency_slot < $available_currency_size; $currency_slot++) { 
                $currency = $this->available_currency[$currency_slot];
                if ($remaining_amount >= $currency)
                {
                    if (isset($breakdown[(string) $currency]))
                    {
                        $breakdown[(string) $currency]++;
                    }
                    else
                    {
                        $breakdown[(string) $currency] = 1;
                    }

                    $remaining_amount -= $currency;

                    break;
                }
                else
                {
                    continue;
                }
            }
        }

        return $breakdown;
    }

}
