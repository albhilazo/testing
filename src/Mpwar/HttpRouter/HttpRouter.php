<?php

namespace Mpwar\HttpRouter;

use Symfony\Component\Yaml\Parser;

class HttpRouter
{

    const PATH_PARAM_REGEX = "/^{(.*)}$/";

    private $routes;




    public function __construct(Parser $yml_parser, $routes_config_path)
    {
        $this->routes = $yml_parser->parse(
            file_get_contents($routes_config_path)
        )['routes'];
    }




    public function getRoute($path)
    {
        foreach ($this->routes as $route_id => $route_attributes)
        {
            $route = $this->checkIfConfiguredRoute($path, $route_id, $route_attributes);

            if (!isset($route))
            {
                continue;
            }

            return $route;
        }
    }




    private function checkIfConfiguredRoute($path, $route_id, $route_attributes)
    {
        if ($this->routeHasNoPath($route_attributes))
        {
            return;
        }

        $exploded_configured_path = $this->explodePath($route_attributes['path']);
        $exploded_received_path   = $this->explodePath($path);

        $configured_path_size = count($exploded_configured_path);
        $received_path_size   = count($exploded_received_path);

        if ($this->pathSizeDoesNotMatch($configured_path_size, $received_path_size))
        {
            return;
        }

        $path_matches = true;
        $parameters   = array();
        for ($path_index = 0; $path_index < $configured_path_size; $path_index++)
        {
            if (preg_match(self::PATH_PARAM_REGEX, $exploded_configured_path[$path_index], $parameter_name))
            {
                $parameters[$parameter_name[1]] = $exploded_received_path[$path_index];
                continue;
            }

            if ($this->pathSectionDoesNotMatch(
                $exploded_configured_path[$path_index],
                $exploded_received_path[$path_index]
            ))
            {
                $path_matches = false;
                break;
            }
        }

        if ($path_matches)
        {
            $route = array();

            $route['route_id'] = $route_id;

            if ($this->pathHasParameters($parameters))
            {
                $route['parameters'] = $parameters;
            }

            return $route;
        }
    }




    private function routeHasNoPath($route)
    {
        return !isset($route['path']);
    }




    private function explodePath($path)
    {
        $exploded_path = explode('/', $path);
        array_shift($exploded_path);

        return $exploded_path;
    }




    private function pathSizeDoesNotMatch($one_size, $another_size)
    {
        return $one_size !== $another_size;
    }




    private function pathSectionDoesNotMatch($one_section, $another_section)
    {
        return $one_section !== $another_section;
    }




    private function pathHasParameters($parameters)
    {
        return !empty($parameters);
    }

}
