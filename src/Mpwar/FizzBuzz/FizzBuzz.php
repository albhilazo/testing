<?php

namespace Mpwar\FizzBuzz;

class FizzBuzz
{
    const INITIAL_VALUE = '';

    private $solvers;

    public function __construct(array $allSolvers)
    {
        $this->solvers = $allSolvers;
    }

    public function calculateResult($inputValue)
    {
        $stackedResult = self::INITIAL_VALUE;

        foreach ($this->solvers as $solver) {
            $stackedResult = $solver->composeStackedResult($inputValue, $stackedResult);
        }

        return $stackedResult;
    }
}