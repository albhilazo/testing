<?php

namespace MpwarUnit\Money;

use Mpwar\Money\Money;
use PHPUnit_Framework_TestCase;

final class MoneyTest extends PHPUnit_Framework_TestCase
{

    const NUMBER = 700;

    const AVAILABLE_CURRENCY = [
        500, 200, 100, 50, 20, 10, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01
    ];

    const NUMBER_BREAKDOWN = [
        '500'  => 1,
        '200'  => 1,
    ];




    protected function tearDown()
    {
        $this->number = null;
        $this->money  = null;
        $this->result = null;
    }




    /** @test */
    public function shouldGetCorrectMoneyBreakdownResultForNumber()
    {
        $this->givenANumber(self::NUMBER);
        $this->havingAMoneyBreakdownSystem();
        $this->whenGettingTheMoneyBreakdownResult();
        $this->thenTheResultShouldMatchTheCorrectBreakdown();
    }




    private function givenANumber($number)
    {
        $this->number = $number;
    }

    private function havingAMoneyBreakdownSystem()
    {
        $this->money = new Money(self::AVAILABLE_CURRENCY);
    }

    private function whenGettingTheMoneyBreakdownResult()
    {
        $this->result = $this->money->getBreakdown($this->number);
    }

    private function thenTheResultShouldMatchTheCorrectBreakdown()
    {
        $this->assertEquals(self::NUMBER_BREAKDOWN, $this->result);
    }

}
