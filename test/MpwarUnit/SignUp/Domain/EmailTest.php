<?php

namespace MpwarUnit\SignUp\Domain;

use Mpwar\SignUp\Domain\Email;
use Mpwar\SignUp\Exception\InvalidEmailException;
use PHPUnit_Framework_TestCase;

final class EmailTest extends PHPUnit_Framework_TestCase
{

    protected function tearDown()
    {
        $this->email        = null;
        $this->email_object = null;
    }


    /**
     * @test
     * @dataProvider invalidEmailProvider
     */
    public function shouldThrowExceptionForAnInvalidEmail($email)
    {
        $this->givenAnInvalidEmail($email);
        $this->thenAnInvalidEmailExceptionShouldBeThrown();
    }

    /**
     * @test
     * @dataProvider validEmailProvider
     */
    public function shouldCreateAnEmailObjectForAValidEmail($email)
    {
        $this->givenAValidEmail($email);
        $this->thenAnEmailObjectIsCreated();
        $this->andTheObjectValueIsCorrect();
    }


    public function invalidEmailProvider()
    {
        return [ ['foobar'], ['foobar.com'], ['foo@bar'], ['foobar@'], ['foobar@.com'], ['@foobar'], ['@foobar.com'] ];
    }

    public function validEmailProvider()
    {
        return [ ['foo@bar.com'], ['foo.bar@baz.com'] ];
    }


    private function givenAValidEmail($email)
    {
        $this->email = $email;
    }

    private function givenAnInvalidEmail($email)
    {
        $this->email = $email;
    }


    private function thenAnInvalidEmailExceptionShouldBeThrown()
    {
        $this->expectException(InvalidEmailException::class);
        new Email($this->email);
    }

    private function thenAnEmailObjectIsCreated()
    {
        $this->email_object = new Email($this->email);
        $this->assertInstanceOf(Email::class, $this->email_object);
    }

    private function andTheObjectValueIsCorrect()
    {
        $this->assertEquals($this->email_object->getValue(), $this->email);
    }

}
