<?php

namespace MpwarUnit\SignUp\Domain;

use Mpwar\SignUp\Domain\Password;
use Mpwar\SignUp\Exception\PasswordTooShortException;
use Mpwar\SignUp\Exception\PasswordMissingUppercaseException;
use Mpwar\SignUp\Exception\PasswordMissingNumbersException;
use PHPUnit_Framework_TestCase;

final class PasswordTest extends PHPUnit_Framework_TestCase
{

    const TOO_SHORT_PASSWORD    = 'Fobr1';
    const NO_UPPERCASE_PASSWORD = 'foobar1';
    const NO_NUMBERS_PASSWORD   = 'Foobar';


    protected function tearDown()
    {
        $this->password        = null;
        $this->password_object = null;
    }


    /**
     * @test
     */
    public function shouldThrowExceptionForAPasswordShorterThanTheMinimum()
    {
        $this->givenAPasswordShorterThanTheMinimum();
        $this->thenAPasswordTooShortExceptionShouldBeThrown();
    }

    /**
     * @test
     */
    public function shouldThrowExceptionForAPasswordWithoutUppercaseCharacters()
    {
        $this->givenAPasswordWithoutUppercaseCharacters();
        $this->thenAPasswordMissingUppercaseExceptionShouldBeThrown();
    }

    /**
     * @test
     */
    public function shouldThrowExceptionForAPasswordWithoutNumbers()
    {
        $this->givenAPasswordWithoutNumbers();
        $this->thenAPasswordMissingNumbersExceptionShouldBeThrown();
    }

    /**
     * @test
     * @dataProvider validPasswordProvider
     */
    public function shouldCreateAPasswordObjectWithHashedValueForAValidPassword($password, $hash)
    {
        $this->givenAValidPassword($password);
        $this->thenAPasswordObjectIsCreated();
        $this->andTheObjectValueIsCorrectHash($hash);
    }


    public function validPasswordProvider()
    {
        return [
            ['Foobar1', 'c435fc3c2e4f7b92e9a648d14813d5203261f060'],
            ['foo2barBaz', 'f3011d2233263fbf083e19fda479bc44ed880585']
        ];
    }


    private function givenAPasswordShorterThanTheMinimum()
    {
        $this->password = self::TOO_SHORT_PASSWORD;
    }

    private function givenAPasswordWithoutUppercaseCharacters()
    {
        $this->password = self::NO_UPPERCASE_PASSWORD;
    }

    private function givenAPasswordWithoutNumbers()
    {
        $this->password = self::NO_NUMBERS_PASSWORD;
    }

    private function givenAValidPassword($password)
    {
        $this->password = $password;
    }


    private function thenAPasswordTooShortExceptionShouldBeThrown()
    {
        $this->expectException(PasswordTooShortException::class);
        new Password($this->password);
    }

    private function thenAPasswordMissingUppercaseExceptionShouldBeThrown()
    {
        $this->expectException(PasswordMissingUppercaseException::class);
        new Password($this->password);
    }

    private function thenAPasswordMissingNumbersExceptionShouldBeThrown()
    {
        $this->expectException(PasswordMissingNumbersException::class);
        new Password($this->password);
    }

    private function thenAPasswordObjectIsCreated()
    {
        $this->password_object = new Password($this->password);
        $this->assertInstanceOf(Password::class, $this->password_object);
    }

    private function andTheObjectValueIsCorrectHash($hash)
    {
        $this->assertEquals($this->password_object->getValue(), $hash);
    }

}
