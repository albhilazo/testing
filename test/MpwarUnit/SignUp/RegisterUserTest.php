<?php

namespace MpwarUnit\SignUp;

use Mpwar\SignUp\RegisterUser;
use Mpwar\SignUp\UserRepository;
use Mpwar\SignUp\EventQueue;
use PHPUnit_Framework_TestCase;

final class RegisterUserTest extends PHPUnit_Framework_TestCase
{

    const VALID_EMAIL    = 'foo@bar.com';
    const VALID_PASSWORD = 'Foobar1';


    protected function tearDown()
    {
        $this->email        = null;
        $this->password     = null;
        $this->repository   = null;
        $this->queue        = null;
        $this->registeruser = null;
    }


    /**
     * @test
     */
    public function shouldRegisterAValidUserAndDispatchEvents()
    {
        $this->givenAValidEmail();
        $this->givenAValidPassword();
        $this->havingAUserRepository();
        $this->havingAnEventQueue();
        $this->havingARegisterUserService();
        $this->thenAUserShouldBeRegistered();
    }


    private function givenAValidEmail()
    {
        $this->email = self::VALID_EMAIL;
    }

    private function givenAValidPassword()
    {
        $this->password = self::VALID_PASSWORD;
    }


    private function havingAUserRepository()
    {
        $this->repository = $this->getMock(UserRepository::class);
        $this->repository
            ->expects($this->once())
            ->method('add');
    }

    private function havingAnEventQueue()
    {
        $this->queue = $this->getMock(EventQueue::class);
        $this->queue
            ->expects($this->exactly(2))
            ->method('dispatch');
    }

    private function havingARegisterUserService()
    {
        $this->registeruser = new RegisterUser($this->repository, $this->queue);
    }


    private function thenAUserShouldBeRegistered()
    {
        $this->registeruser->registerNew($this->email, $this->password);
    }

}
