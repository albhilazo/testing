<?php

namespace MpwarUnit\HttpRouter;

use Mpwar\HttpRouter\HttpRouter;
use Symfony\Component\Yaml\Parser;
use PHPUnit_Framework_TestCase;

final class HttpRouterTest extends PHPUnit_Framework_TestCase
{

    const ROUTES_CONFIG = __DIR__.'/routes.yml';

    const UNREGISTERED_PATH = '/foo';
    const SIMPLE_PATH = '/home';
    const PATH_WITH_SINGLE_PARAMETER = '/students/1234';
    const PATH_WITH_MULTIPLE_PARAMETERS = '/students/1234/scores/5678';

    const NO_ROUTE = null;

    const SIMPLE_PATH_ROUTE    = array(
        'route_id' => 'home_page'
    );
    const SINGLE_PARAMETER_ROUTE = array(
        'route_id'   => 'students_resource',
        'parameters' => array(
            'id' => '1234'
        )
    );
    const MULTIPLE_PARAMETERS_ROUTE = array(
        'route_id'   => 'students_scores_resource',
        'parameters' => array(
            'student_id' => '1234',
            'score_id'   => '5678'
        )
    );




    protected function tearDown()
    {
        $this->parser = null;
        $this->router = null;
        $this->result = null;
    }




    /** @test */
    public function shouldGetNoResultForUnregisteredPath()
    {
        $this->givenAYamlParser();
        $this->havingARoutingSystem();
        $this->whenGettingTheRouteForAnUnregisteredPath();
        $this->thenTheResultShouldBeNull();
    }

    /** @test */
    public function shouldGetResultForSimplePath()
    {
        $this->givenAYamlParser();
        $this->havingARoutingSystem();
        $this->whenGettingTheRouteForASimplePath();
        $this->thenTheResultShouldBeTheOneForSimplePath();
    }

    /** @test */
    public function shouldGetResultForPathWithSingleParameter()
    {
        $this->givenAYamlParser();
        $this->havingARoutingSystem();
        $this->whenGettingTheRouteForAPathWithSingleParameter();
        $this->thenTheResultShouldBeTheOneForAPathWithSingleParameter();
    }

    /** @test */
    public function shouldGetResultForPathWithMultipleParameters()
    {
        $this->givenAYamlParser();
        $this->havingARoutingSystem();
        $this->whenGettingTheRouteForAPathWithMultipleParameters();
        $this->thenTheResultShouldBeTheOneForAPathWithMultipleParameters();
    }




    private function givenAYamlParser()
    {
        $this->parser = new Parser();
    }

    private function havingARoutingSystem()
    {
        $this->router = new HttpRouter(
            $this->parser,
            self::ROUTES_CONFIG
        );
    }

    private function whenGettingTheRouteForAnUnregisteredPath()
    {
        $this->result = $this->router->getRoute(self::UNREGISTERED_PATH);
    }

    private function whenGettingTheRouteForASimplePath()
    {
        $this->result = $this->router->getRoute(self::SIMPLE_PATH);
    }

    private function whenGettingTheRouteForAPathWithSingleParameter()
    {
        $this->result = $this->router->getRoute(self::PATH_WITH_SINGLE_PARAMETER);
    }

    private function whenGettingTheRouteForAPathWithMultipleParameters()
    {
        $this->result = $this->router->getRoute(self::PATH_WITH_MULTIPLE_PARAMETERS);
    }

    private function thenTheResultShouldBeNull()
    {
        $this->assertEquals(self::NO_ROUTE, $this->result);
    }

    private function thenTheResultShouldBeTheOneForSimplePath()
    {
        $this->assertEquals(self::SIMPLE_PATH_ROUTE, $this->result);
    }

    private function thenTheResultShouldBeTheOneForAPathWithSingleParameter()
    {
        $this->assertEquals(self::SINGLE_PARAMETER_ROUTE, $this->result);
    }

    private function thenTheResultShouldBeTheOneForAPathWithMultipleParameters()
    {
        $this->assertEquals(self::MULTIPLE_PARAMETERS_ROUTE, $this->result);
    }

}
