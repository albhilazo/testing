<?php

namespace MpwarUnit\Blog;

use Mpwar\Blog\PublishPost;
use Mpwar\Blog\DataBase\PersistNewPost;
use Mpwar\Blog\NotificationQueue\NewPostEvent;
use PHPUnit_Framework_TestCase;

final class PublishPostTest extends PHPUnit_Framework_TestCase
{

    const TITLE_LESS_THAN_50_CHARS  = 'Lorem ipsum dolor sit amet, consectetuer adipisci';
    const TITLE_MORE_THAN_50_CHARS  = 'Lorem ipsum dolor sit amet, consectetuer adipiscing';
    const BODY_LESS_THAN_1000_CHARS = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. ';
    const BODY_MORE_THAN_1000_CHARS = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Na';

    const PUBLISH_NOW_DEFAULT = false;
    const PUBLISH_NOW_NO      = false;
    const PUBLISH_NOW_YES     = true;




    protected function tearDown()
    {
        $this->title       = null;
        $this->body        = null;
        $this->publishNow  = null;
        $this->persist     = null;
        $this->event       = null;
        $this->publishPost = null;
    }




    /**
     * @test
     * @dataProvider titleOrBodyIsInvalidProvider
     * @expectedException Mpwar\Blog\Exception\ValidationError
     */
    public function shouldGetExceptionForInvalidFields($title, $body)
    {
        $this->givenAnyInvalidField($title, $body);
        $this->givenAnyPublishTime();
        $this->havingAPostPublisher();
        $this->thenAValidationErrorExceptionShouldBeThrownWhenTryingToPublishAPost();
    }




    public function givenAnyInvalidField($title, $body)
    {
        $this->title = $title;
        $this->body  = $body;
    }

    public function givenAnyPublishTime()
    {
        $this->publishNow = self::PUBLISH_NOW_DEFAULT;
    }

    public function havingAPostPublisher()
    {
        $this->publishPost = new PublishPost();
    }

    public function thenAValidationErrorExceptionShouldBeThrownWhenTryingToPublishAPost()
    {
        $this->publishPost->__invoke(
            $this->title,
            $this->body,
            $this->publishNow
        );
    }

    private function givenAPersistService()
    {
        /*$this->persist = $this->getMock(PersistNewPost::class);
        $this->persist
            ->method('persist');*/
    }

    public function titleOrBodyIsInvalidProvider()
    {
        return [
            [self::TITLE_MORE_THAN_50_CHARS, self::BODY_LESS_THAN_1000_CHARS],
            [self::TITLE_LESS_THAN_50_CHARS, self::BODY_MORE_THAN_1000_CHARS],
            [self::TITLE_MORE_THAN_50_CHARS, self::BODY_MORE_THAN_1000_CHARS],
        ];
    }

}
