<?php

namespace MpwarIntegration\SignUp\Dummy;

use Mpwar\SignUp\EventQueue;
use Mpwar\SignUp\Event\Event;

class DummyEventQueue implements EventQueue
{

    private $events = [];

    public function dispatch(Event $event)
    {
        $this->events[$event->getName()] = $event;
    }

    public function getEvents()
    {
        return $this->events;
    }

}
