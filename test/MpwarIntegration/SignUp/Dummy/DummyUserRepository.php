<?php

namespace MpwarIntegration\SignUp\Dummy;

use Mpwar\SignUp\UserRepository;
use Mpwar\SignUp\Domain\User;

class DummyUserRepository implements UserRepository
{

    private $users = [];

    public function add(User $user)
    {
        $this->users[$user->getEmail()->getValue()] = $user;
    }

    public function getUsers()
    {
        return $this->users;
    }

}
