<?php

namespace MpwarIntegration\SignUp\Context;

use Behat\Behat\Context\SnippetAcceptingContext;
use Mpwar\SignUp\RegisterUser;
use MpwarIntegration\SignUp\Dummy\DummyUserRepository;
use MpwarIntegration\SignUp\Dummy\DummyEventQueue;
use PHPUnit_Framework_Assert;

final class DefaultContext implements SnippetAcceptingContext
{

    /**
     * @Given an email :email
     */
    public function givenAnEmail($email)
    {
        $this->inputEmail = $email;
    }

    /**
     * @Given a password :password
     */
    public function givenAPassword($password)
    {
        $this->inputPassword = $password;
    }

    /**
     * @When executing the RegisterUser service
     */
    public function executingTheRegisterUserService()
    {
        $this->repository = new DummyUserRepository();
        $this->queue = new DummyEventQueue();

        $registeruser = new RegisterUser($this->repository, $this->queue);

        try {
            $registeruser->registerNew($this->inputEmail, $this->inputPassword);
        } catch (\Exception $e) {
            $this->exception = $e;
        }
    }

    /**
     * @Then a user should be registered
     */
    public function aUserShouldBeRegistered()
    {
        $users = $this->repository->getUsers();
        PHPUnit_Framework_Assert::assertArrayHasKey(
            $this->inputEmail,
            $users
        );
        PHPUnit_Framework_Assert::assertEquals(
            sha1($this->inputPassword),
            $users[$this->inputEmail]->getPassword()->getValue()
        );
    }

    /**
     * @Then an :eventName event is dispatched
     */
    public function theEventIsDispatched($eventName)
    {
        $events = $this->queue->getEvents();
        PHPUnit_Framework_Assert::assertArrayHasKey(
            $eventName,
            $events
        );
        PHPUnit_Framework_Assert::assertInstanceOf(
            '\Mpwar\SignUp\Event\Event',
            $events[$eventName]
        );
    }

    /**
     * @Then an :exceptionName exception should be thrown
     */
    public function anExceptionShouldBeThrown($exceptionName)
    {
        PHPUnit_Framework_Assert::assertInstanceOf(
            $exceptionName,
            $this->exception
        );
    }

    /**
     * @AfterScenario
     */
    public function clean()
    {
        $this->inputEmail    = null;
        $this->inputPassword = null;
        $this->repository    = null;
        $this->queue         = null;
        $this->exception     = null;
    }

}
