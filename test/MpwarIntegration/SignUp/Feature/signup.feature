Feature: I can register a user in the repository and dispatch respective events
    As a sane person
    I want to make sure that registration is correct and events are dispatched
    So that the new user is registered correctly

    Scenario: Invalid email, signup error
        Given an email "foobar.com"
        And a password "Foobar1"
        When executing the RegisterUser service
        Then an "\Mpwar\SignUp\Exception\InvalidEmailException" exception should be thrown

    Scenario: Invalid password, signup error
        Given an email "foo@bar.com"
        And a password "Fobr1"
        When executing the RegisterUser service
        Then an "\Mpwar\SignUp\Exception\InvalidPasswordException" exception should be thrown

    Scenario: Signup successful
        Given an email "foo@bar.com"
        And a password "Foobar1"
        When executing the RegisterUser service
        Then a user should be registered
        And an "NewUser" event is dispatched
        And an "AutoLoginUser" event is dispatched
