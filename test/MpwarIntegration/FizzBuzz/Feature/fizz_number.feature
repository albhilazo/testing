Feature: I can detect fizz numbers
    As a sane person
    I want to make sure that fizz numbers can be recognised
    So that I can pass testing

    Scenario: default case
        Given a 3
        When executing the fizz buzz service
        Then the result should be 'fizz'

    Scenario: another case
        Given a 6
        When executing the fizz buzz service
        Then the result should be 'fizz'

    Scenario: another case
        Given a 12
        When executing the fizz buzz service
        Then the result should be 'fizz'
